VERSION 5.00
Object = "{90F3D7B3-92E7-44BA-B444-6A8E2A3BC375}#1.0#0"; "Actskin4.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frm_registro 
   BackColor       =   &H00FFFF00&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Mercancia"
   ClientHeight    =   9030
   ClientLeft      =   3285
   ClientTop       =   945
   ClientWidth     =   12735
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "@Arial Unicode MS"
      Size            =   15.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9030
   ScaleWidth      =   12735
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Administrador"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   8280
      TabIndex        =   31
      Top             =   1680
      Width           =   4335
      Begin VB.TextBox txt_password 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "@Arial Unicode MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1320
         TabIndex        =   36
         Text            =   "PASSWORD"
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox txt_user 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "@Arial Unicode MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1320
         TabIndex        =   35
         Text            =   "USER"
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmd_cambiarpass 
         Caption         =   "CambiarPassword"
         BeginProperty Font 
            Name            =   "@Arial Unicode MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   2760
         TabIndex        =   34
         Top             =   240
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel adm_usr 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Mercancia.frx":0000
         TabIndex        =   32
         Top             =   240
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "Mercancia.frx":006E
         TabIndex        =   33
         Top             =   600
         Width           =   1215
      End
   End
   Begin VB.TextBox text 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DataField       =   "Precio"
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   1
      EndProperty
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   1560
      TabIndex        =   2
      Top             =   2280
      Width           =   4095
   End
   Begin VB.TextBox text 
      BorderStyle     =   0  'None
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   1
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   2
      Left            =   3480
      TabIndex        =   26
      Top             =   2880
      Width           =   2175
   End
   Begin VB.TextBox text 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   1
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   3
      Left            =   1920
      TabIndex        =   25
      Top             =   3480
      Width           =   3735
   End
   Begin VB.TextBox text 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DataField       =   "Descripcion"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   1560
      TabIndex        =   1
      Top             =   1680
      Width           =   4095
   End
   Begin VB.CommandButton btn_editar 
      Caption         =   "Guardar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4680
      TabIndex        =   19
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "CARGAR DB"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11400
      TabIndex        =   11
      Top             =   3000
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Frame frm_botones 
      Caption         =   "Controles"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   4560
      TabIndex        =   16
      Top             =   120
      Width           =   8055
      Begin VB.TextBox txt_busca 
         Alignment       =   2  'Center
         Height          =   465
         Left            =   3000
         TabIndex        =   30
         Text            =   "BUSCAR POR DESCRIPCION"
         Top             =   360
         Width           =   4815
      End
      Begin VB.CommandButton btn_eliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "@Arial Unicode MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1560
         TabIndex        =   18
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton btn_agregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "@Arial Unicode MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   1335
      End
      Begin MSAdodcLib.Adodc Adodc1 
         Height          =   495
         Left            =   1560
         Top             =   840
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   873
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "@Arial Unicode MS"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Height          =   4935
      Left            =   120
      TabIndex        =   10
      Top             =   4080
      Width           =   12495
      _ExtentX        =   22040
      _ExtentY        =   8705
      _Version        =   393216
      AllowUpdate     =   0   'False
      Appearance      =   0
      BorderStyle     =   0
      ColumnHeaders   =   -1  'True
      HeadLines       =   1
      RowHeight       =   25
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "@Arial Unicode MS"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "MERCANCIA FRIGO TECNO ORIENTE, C.A"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   8202
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   8202
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.TextBox text 
      BorderStyle     =   0  'None
      DataField       =   "Existencia"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Index           =   7
      Left            =   7560
      TabIndex        =   4
      Top             =   2280
      Width           =   615
   End
   Begin VB.TextBox text 
      BorderStyle     =   0  'None
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   1
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   6
      Left            =   9120
      TabIndex        =   9
      Top             =   3480
      Width           =   2775
   End
   Begin VB.TextBox text 
      BorderStyle     =   0  'None
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   1
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   5
      Left            =   8400
      TabIndex        =   7
      Top             =   2880
      Width           =   2775
   End
   Begin VB.TextBox text 
      BorderStyle     =   0  'None
      DataField       =   "Vendido"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   4
      Left            =   7320
      TabIndex        =   3
      Top             =   1680
      Width           =   855
   End
   Begin VB.PictureBox Picture1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   0
      Picture         =   "Mercancia.frx":00DE
      ScaleHeight     =   1515
      ScaleWidth      =   4395
      TabIndex        =   0
      Top             =   0
      Width           =   4455
      Begin VB.CommandButton Command2 
         Caption         =   "test"
         Height          =   495
         Left            =   3600
         TabIndex        =   37
         Top             =   0
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   3360
      OleObjectBlob   =   "Mercancia.frx":18E60
      Top             =   720
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   4
      Left            =   5760
      OleObjectBlob   =   "Mercancia.frx":4E8DB
      TabIndex        =   12
      Top             =   2280
      Width           =   1695
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   5
      Left            =   5760
      OleObjectBlob   =   "Mercancia.frx":4E94F
      TabIndex        =   13
      Top             =   1680
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   6
      Left            =   5760
      OleObjectBlob   =   "Mercancia.frx":4E9BF
      TabIndex        =   14
      Top             =   2880
      Width           =   2535
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   7
      Left            =   5760
      OleObjectBlob   =   "Mercancia.frx":4EA41
      TabIndex        =   15
      Top             =   3480
      Width           =   3255
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   0
      Left            =   120
      OleObjectBlob   =   "Mercancia.frx":4EACF
      TabIndex        =   24
      Top             =   1680
      Width           =   1335
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   1
      Left            =   120
      OleObjectBlob   =   "Mercancia.frx":4EB3F
      TabIndex        =   27
      Top             =   2280
      Width           =   1335
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   2
      Left            =   120
      OleObjectBlob   =   "Mercancia.frx":4EBAB
      TabIndex        =   28
      Top             =   2880
      Width           =   3255
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   375
      Index           =   3
      Left            =   120
      OleObjectBlob   =   "Mercancia.frx":4EC35
      TabIndex        =   29
      Top             =   3480
      Width           =   1695
   End
   Begin VB.Label Producto 
      BackStyle       =   0  'Transparent
      Caption         =   "Producto:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   23
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Precio:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   22
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Incremento 12% de IVA:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   21
      Top             =   2880
      Width           =   3375
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Precio Total:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   20
      Top             =   3480
      Width           =   2055
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Total en Existencia Bsf:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   8
      Top             =   3480
      Width           =   3255
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Total Vendido Bsf:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5880
      TabIndex        =   6
      Top             =   2880
      Width           =   2655
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Vendidos:"
      BeginProperty Font 
         Name            =   "@Arial Unicode MS"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5880
      TabIndex        =   5
      Top             =   1680
      Width           =   1635
   End
End
Attribute VB_Name = "frm_registro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim i As Integer
Dim hpas As Integer

Sub cargardb()
'Adodc1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Login.SelectorDB.Path + "\" + Login.SelectorDB.FileName & ";Persist Security Info=False"
Adodc1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path + "\TecnoOriente2.mdb" & ";Persist Security Info=False"
Adodc1.CursorType = adOpenDynamic
Adodc1.RecordSource = "Mercancia"
Adodc1.Refresh
End Sub
Sub cargartabla()
Set DataGrid1.DataSource = Adodc1
End Sub
Sub cargarcfg()
Dim rutacfg As String
rutacfg = App.Path + "\Configuracion.ini"
Dim cfg As New cINI
cfg.INIPath = rutacfg
For i = 0 To DataGrid1.Columns.Count - 1
DataGrid1.Columns.Item(i).Width = cfg.getValue(rutacfg, "Columnas", Val(i))
Next
End Sub
Sub loadadmin()
txt_user.text = Login.txt_user
txt_password.text = Login.txt_password
End Sub
Sub fadein()
If Not Transparencia(Me.hWnd, 0) = 0 Then
      
    MsgBox " Esta funci�n Api no es soportada en Versiones" _
           & "anteriores a windows 2000", vbCritical
    Me.Show
Else
  
    ' Gradua la transparencia del formulario hasta hacerla visible _
     es decir desde el valor 0 hasta el 255
      
    'desactiva el Formulario
    Me.Enabled = False
    Me.Show
      
    For i = 0 To 255 Step 2
        ' Va aplicando los distintos valores y grados de transparencia al form
        Call Transparencia(Me.hWnd, i)
        DoEvents
    Next
      
    'reactiva la ventana
    Me.Enabled = True
       
End If
End Sub
Private Sub Adodc1_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)
   On Local Error Resume Next
    Adodc1.Caption = Adodc1.Recordset!Descripcion
    Err = 0
End Sub

Private Sub btn_agregar_Click()
btn_editar.Enabled = True
Adodc1.Recordset.AddNew
End Sub

Private Sub btn_editar_Click()
On Error Resume Next
Adodc1.Recordset.Update
MsgBox "Cambios Almacenados!", vbInformation
Adodc1.Recordset.MoveFirst
Adodc1.Refresh
btn_editar.Enabled = False

End Sub

Private Sub btn_eliminar_Click()
On Error Resume Next
With Adodc1.Recordset
.Filter = ("id='" & !id & "'")
resp = MsgBox("�Seguro que desea Eliminar la Mercancia seleccionada?", vbCritical + vbYesNo, "Eliminar")
If resp = vbYes Then
.Delete
MsgBox "Registro Eliminado!", vbInformation
'Adodc1.Recordset.MoveFirst
Else
End If
End With
Adodc1.Refresh
End Sub

Sub cargar_descripciones()
With Adodc1.Recordset
Do Until .EOF
Combo1.AddItem ![Descripcion]
.MoveNext
Loop
End With

End Sub

Private Sub cmd_cambiarpass_Click()
With Login.DB.Recordset
.Fields("Password").Value = txt_password.text
.Update
End With
cmd_cambiarpass.Enabled = False
End Sub

Private Sub Command1_Click()
cargardb
cargarcfg
End Sub

Sub guardarcfg()
Debug.Print "VALOR DE WIDTH: " & DataGrid1.Columns.Item(ColIndex).Width
Dim rutacfg As String
rutacfg = App.Path + "\Configuracion.ini"
Dim cfg As New cINI
cfg.INIPath = rutacfg
For i = 0 To DataGrid1.Columns.Count - 1
cfg.writeValue rutacfg, "Columnas", Val(i), DataGrid1.Columns.Item(i).Width
Next
End Sub
Private Sub Form_Load()
Skin1.ApplySkin Me.hWnd
cargardb
cargartabla
cargarcfg
loadadmin
fadein
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If MsgBox("Cerrar sesion?", vbQuestion + vbYesNo, "Salir?") = vbYes Then
guardarcfg
Adodc1.Recordset.Close
Cancel = 0

If Not Transparencia(Me.hWnd, 0) = 0 Then
    Exit Sub
Else
    ' Gradua la transparencia del formulario hasta hacerla invisible _
      y luego se descarga, desde el valor 255 hasta el 0
    For i = 255 To 0 Step -3
        DoEvents
        Call Transparencia(Me.hWnd, i)
        DoEvents
    Next
      
End If
Login.Show
Else
Cancel = 1
End If
End Sub

Sub buscar()
If txt_busca.text = "" Then
Else
criterio = "Descripcion like '*" & txt_busca.text & "*'"
Adodc1.Recordset.MoveNext
If Not Adodc1.Recordset.EOF Then
Adodc1.Recordset.Find criterio
End If
If Adodc1.Recordset.EOF Then
Adodc1.Recordset.MoveFirst
Adodc1.Recordset.Find criterio
If Adodc1.Recordset.EOF Then
Adodc1.Recordset.MoveLast
MsgBox "Registro no encontrado!", vbCritical
End If
End If
End If
End Sub


Private Sub Picture1_DblClick()

If hpas = 8 Then
MsgBox "Esta aplicacion fue dise�ada entera y completamente por David Jose Castillo" & vbNewLine & _
"Con bastante esfuerzo para el estudiante Felix!", vbInformation, "Huevo de Pascua!"
Else
hpas = hpas + 1
End If
Debug.Print hpas

End Sub

Private Sub text_Change(Index As Integer)
text(2).text = Val(text(1).text) * 0.12 ' calcular impuesto del 12%
text(3).text = Val(text(1).text) + Val(text(2).text) ' sumo el resultado de la multiplicacion
text(5).text = Val(text(3).text) * Val(text(4).text)
text(6).text = Val(text(3).text) * Val(text(7).text)

End Sub

Private Sub text_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case Index
Case 0
btn_editar.Enabled = True
Case 1
KeyAscii = NumericValidation(KeyAscii) 'llamo a la rutina de validacion para que no
                                      'deje colocar letras en este campo numerico.
                                      btn_editar.Enabled = True
Case 4
KeyAscii = NumericValidation(KeyAscii)
                                      btn_editar.Enabled = True
Case 7
KeyAscii = NumericValidation(KeyAscii)
Case Else
If KeyAscii = 13 Then
text(4).SetFocus
End If

End Select

End Sub

Private Sub txt_busca_GotFocus()
txt_busca.text = ""
End Sub

Private Sub txt_busca_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
buscar
End If
End Sub

Private Sub txt_busca_LostFocus()
txt_busca.text = "BUSCAR POR DESCRIPCION"
End Sub
Private Sub txt_password_KeyPress(KeyAscii As Integer)
cmd_cambiarpass.Enabled = True
End Sub
