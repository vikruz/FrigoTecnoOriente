Attribute VB_Name = "Validadores"

Public Function AlphaValidation(KeyAscii As Integer) As Integer

If ((KeyAscii > 64 And KeyAscii < 91) _
            Or (KeyAscii > 96 And KeyAscii < 123) _
                    Or KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13) Then
    
    AlphaValidation = KeyAscii
Else
    AlphaValidation = 0
End If

End Function
Public Function NumericValidation(KeyAscii As Integer, Optional AllowDecimal As Boolean, Optional text As String) As Integer
If AllowDecimal Then

    If KeyAscii = 46 Then
                If InStr(text, ".") = 0 Then
                    NumericValidation = KeyAscii
                    Exit Function
                Else
                    NumericValidation = 0
                End If
        End If
End If

If KeyAscii > 47 And KeyAscii < 58 Or KeyAscii = 8 Or KeyAscii = 13 Then
    NumericValidation = KeyAscii
Else
    NumericValidation = 0
End If
End Function

Public Function NumericValidation2(KeyAscii As Integer, Optional AllowDecimal As Boolean, Optional text As String) As Integer
If AllowDecimal Then

    If KeyAscii = 46 Then
                If InStr(text, ".") = 0 Then
                    NumericValidation2 = KeyAscii
                    Exit Function
                Else
                    NumericValidation2 = 0
                End If
        End If
End If

If KeyAscii > 47 And KeyAscii < 58 Or KeyAscii = 8 Or KeyAscii = 13 Then
    NumericValidation2 = KeyAscii
Else
    NumericValidation2 = 0
End If
End Function

Public Sub LIMPIAR(F As Form)
Dim C As Control
For Each C In F
If TypeOf C Is TextBox Or TypeOf C Is ComboBox Then C = ""
If TypeOf C Is OptionButton Then C.Value = False
If TypeOf C Is CheckBox Then C.Value = 0
If TypeOf C Is ListBox Then C.Clear
Next
End Sub
