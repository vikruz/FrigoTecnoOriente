VERSION 5.00
Object = "{90F3D7B3-92E7-44BA-B444-6A8E2A3BC375}#1.0#0"; "Actskin4.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "Progressbar-xp.ocx"
Begin VB.Form Login 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conectar Base de datos"
   ClientHeight    =   3720
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   8415
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   4  'Icon
   ScaleHeight     =   3720
   ScaleWidth      =   8415
   StartUpPosition =   2  'CenterScreen
   Begin Proyecto2.XP_ProgressBar progreso 
      Height          =   495
      Left            =   0
      TabIndex        =   9
      Top             =   3240
      Visible         =   0   'False
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   873
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   16777088
      Max             =   500
      Scrolling       =   9
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   7320
      OleObjectBlob   =   "Registro de Mercancia.frx":0000
      Top             =   120
   End
   Begin MSAdodcLib.Adodc DB 
      Height          =   330
      Left            =   7080
      Top             =   1440
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\DaviDJ\PROGRAMACION\vb6\Felix2\registro.mdb;Persist Security Info=False"
      OLEDBString     =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\DaviDJ\PROGRAMACION\vb6\Felix2\registro.mdb;Persist Security Info=False"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "select * from Usuarios"
      Caption         =   "DB"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   1575
      Left            =   2400
      Picture         =   "Registro de Mercancia.frx":35A7B
      ScaleHeight     =   1575
      ScaleWidth      =   4455
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
   Begin VB.Frame frm_selectdb 
      Caption         =   "Base de datos"
      Height          =   1695
      Left            =   0
      TabIndex        =   4
      Top             =   1560
      Width           =   8415
      Begin VB.FileListBox SelectorDB 
         Appearance      =   0  'Flat
         Height          =   1020
         Left            =   2400
         Pattern         =   "*.mdb"
         TabIndex        =   11
         Top             =   480
         Width           =   5655
      End
      Begin VB.CommandButton Command1 
         Caption         =   "NUEVA DB"
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   960
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.CommandButton cmd_conectardb 
         Caption         =   "CONECTAR"
         Height          =   495
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame frm_auth 
      Caption         =   "Autenticar"
      Height          =   1695
      Left            =   0
      TabIndex        =   5
      Top             =   1560
      Visible         =   0   'False
      Width           =   8415
      Begin ACTIVESKINLibCtl.SkinLabel status 
         Height          =   495
         Left            =   6240
         OleObjectBlob   =   "Registro de Mercancia.frx":4E7FD
         TabIndex        =   12
         Top             =   1080
         Width           =   2055
      End
      Begin VB.CommandButton cmd_Login 
         Caption         =   "ENTRAR"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6240
         TabIndex        =   3
         Top             =   480
         Width           =   1815
      End
      Begin VB.TextBox txt_user 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   495
         Left            =   2160
         TabIndex        =   1
         Top             =   480
         Width           =   3975
      End
      Begin VB.TextBox txt_password 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   495
         IMEMode         =   3  'DISABLE
         Left            =   2160
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1080
         Width           =   3975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   600
         OleObjectBlob   =   "Registro de Mercancia.frx":4E877
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "Registro de Mercancia.frx":4E8E5
         TabIndex        =   7
         Top             =   960
         Width           =   1815
      End
   End
End
Attribute VB_Name = "Login"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim i As Integer
Sub fadein()
If Not Transparencia(Me.hWnd, 0) = 0 Then
      
    MsgBox " Esta funci�n Api no es soportada en Versiones" _
           & "anteriores a windows 2000", vbCritical
    Me.Show
Else
  
    ' Gradua la transparencia del formulario hasta hacerla visible _
     es decir desde el valor 0 hasta el 255
      
    'desactiva el Formulario
    Me.Enabled = False
    Me.Show
      
    For i = 0 To 255 Step 2
        ' Va aplicando los distintos valores y grados de transparencia al form
        Call Transparencia(Me.hWnd, i)
        DoEvents
    Next
      
    'reactiva la ventana
    Me.Enabled = True
       
End If
End Sub
Function Autenticar_admin(ByVal Usuario As String, ByVal Password As String) As Boolean
On Error Resume Next
DB.RecordSource = "select * from Administrador where Usuario='" + Usuario + "' and Password='" + Password + "'"
DB.Refresh
If DB.Recordset.EOF Then
Autenticar_admin = False
Else
Autenticar_admin = True
End If
End Function
Sub SetConexion(ByVal rutadb As String)
'rutadb = App.Path + "\registro.mdb"
If SelectorDB.Filename = "" Then
MsgBox "Debes seleccionar una base de datos de la lista! ", vbCritical, App.Title
Else
DB.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & rutadb & ";Persist Security Info=False"
progreso.Visible = True
For i = progreso.Min To progreso.Max
progreso.Value = i
Next
progreso.Visible = False
status.Caption = "Conectado a: " & SelectorDB.Filename

frm_selectdb.Visible = False
frm_auth.Visible = True
End If
End Sub

Private Sub cmd_conectardb_Click()
SetConexion SelectorDB.Path + "\" + SelectorDB.Filename
End Sub

Private Sub cmd_Login_Click()
If Autenticar_admin(txt_user, txt_password) = True Then
frm_registro.Caption = "Bienvenido al sistema de inventario " & txt_user.text & " " & App.Title
Me.Hide
frm_registro.Show
'DB.Recordset.Close
Else
MsgBox "Usuario o contrase�a invalidos!", vbCritical, App.Title
End If
End Sub
Private Sub Form_Load()
SelectorDB.Path = App.Path
Skin1.ApplySkin Me.hWnd
fadein
txt_password.text = ""
End Sub
Private Sub Form_Resize()
txt_password.text = ""
End Sub



Private Sub txt_password_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If Autenticar_admin(txt_user, txt_password) = True Then
MsgBox "Bienvenido al sistema de inventario " & txt_user.text, vbInformation, App.Title
Me.Hide
frm_registro.Show
'DB.Recordset.Close
Else
MsgBox "Usuario o contrase�a invalidos!", vbCritical, App.Title
End If
End If
End Sub

