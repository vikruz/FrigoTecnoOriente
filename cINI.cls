VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cINI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

DefInt A-Z
' --------------------------------------------------------------------------
' \\ Guardar y leer datos desde un archivo INI
' --------------------------------------------------------------------------

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Long

Const ENCRYPT = 1
Const DECRYPT = 2

Const CLAVE         As String = "Clave"
Const SECCION       As String = "Clave"
Private mINIPath    As String

'---------------------------------------------------------------------
' Harvey Triana -  para encriptar y desencriptar cadenas
'---------------------------------------------------------------------

Function Encriptar( _
    UserKey As String, text As String, Action As Single _
    ) As String
    Dim UserKeyX As String
    Dim Temp     As Integer
    Dim Times    As Integer
    Dim i        As Integer
    Dim J        As Integer
    Dim n        As Integer
    Dim rtn      As String
    
    If text = vbNullString Or UserKey = vbNullString Then
       Encriptar = vbNullString
       Exit Function
    End If
    '//Get UserKey characters
    n = Len(UserKey)
    ReDim UserKeyASCIIS(1 To n)
    For i = 1 To n
        UserKeyASCIIS(i) = Asc(Mid$(UserKey, i, 1))
    Next
        
    '//Get Text characters
    ReDim TextASCIIS(Len(text)) As Integer
    For i = 1 To Len(text)
        TextASCIIS(i) = Asc(Mid$(text, i, 1))
    Next
    
    '//Encryption/Decryption
    If Action = ENCRYPT Then
       For i = 1 To Len(text)
           J = IIf(J + 1 >= n, 1, J + 1)
           Temp = TextASCIIS(i) + UserKeyASCIIS(J)
           If Temp > 255 Then
              Temp = Temp - 255
           End If
           rtn = rtn + Chr$(Temp)
       Next
    ElseIf Action = DECRYPT Then
       For i = 1 To Len(text)
           J = IIf(J + 1 >= n, 1, J + 1)
           Temp = TextASCIIS(i) - UserKeyASCIIS(J)
           If Temp < 0 Then
              Temp = Temp + 255
           End If
           rtn = rtn + Chr$(Temp)
       Next
    End If
    
    '//Return
    Encriptar = rtn
End Function

Function getValue(path_ini As String, SECCION As String, Key As String, Optional Default As Variant = "") As String

On Error GoTo error_handler

    Dim buffer As String * 256
    Dim ret As Long

    ret = GetPrivateProfileString(SECCION, Key, Default, buffer, Len(buffer), path_ini)
    getValue = Left$(buffer, ret)
    
Exit Function
error_handler:
MsgBox Err.Description, vbCritical

End Function

Function writeValue(path_ini As String, SECCION As String, Key As String, Valor As Variant) As String
On Error GoTo error_handler

    Dim ret As Long
    ret = WritePrivateProfileString(SECCION, Key, Valor, path_ini)
   
Exit Function
error_handler:
MsgBox Err.Description, vbCritical
   
End Function

Property Get INIPath() As String
    INIPath = mINIPath
End Property
Property Let INIPath(sPath As String)
    mINIPath = sPath
End Property

