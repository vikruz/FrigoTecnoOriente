VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form frmbackup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Backup Database"
   ClientHeight    =   3450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4230
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3450
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   3165
      Width           =   4230
      _ExtentX        =   7461
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   25400
            MinWidth        =   25400
         EndProperty
      EndProperty
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   4230
      Top             =   180
   End
   Begin VB.CommandButton cmdunpack 
      BackColor       =   &H80000005&
      Caption         =   "Unpack Database"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2070
      Width           =   1335
   End
   Begin VB.CommandButton cmdbackup 
      BackColor       =   &H80000009&
      Caption         =   "Backup Database"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   180
      MaskColor       =   &H00000000&
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   810
      Width           =   1335
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   180
      Visible         =   0   'False
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
End
Attribute VB_Name = "frmbackup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Huffman As clsHuffman
Private Sub cmdbackup_Click()
On Error Resume Next
    MkDir App.Path & "\Backup"
    
ProgressBar1.Visible = True
Timer1.Enabled = True
ProgressBar1.Value = 0
StatusBar1.Panels(1).text = ""
Timer1_Timer
Call Huffman.EncodeFile(App.Path & "\3S Pharma.mdb", App.Path & "\backup\db.bkp")

End Sub

Private Sub cmdunpack_Click()
ProgressBar1.Visible = True
Timer1.Enabled = True
ProgressBar1.Value = 20
On Error GoTo report
Call Huffman.DecodeFile(App.Path & "\backup\db.bkp", App.Path & "\3S Pharma.mdb")
MsgBox "Database is restored from Backup Successfully"
'admin
report:
If Err.Number = 75 Then
MsgBox "Database is Open and can't be replaced from the Backup"
Exit Sub
End If
End Sub

Private Sub Form_Load()
Set Huffman = New clsHuffman
End Sub



Private Sub Timer1_Timer()
ProgressBar1.Value = ProgressBar1.Value + 10
StatusBar1.Panels(1).text = "Compressing Database"
If ProgressBar1.Value = 100 Then
Timer1.Enabled = False
ProgressBar1.Value = 0
StatusBar1.Panels(1).text = ""
End If

End Sub
